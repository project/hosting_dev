<?php
/**
 * @file
 * Expose the reinstall feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_dev_hosting_feature() {
  $features = array();

  $features['reinstall'] = array(
    'title' => t('Site reinstall'),
    'description' => t('Provides a Hosting task to reinstall sites.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_site_reinstall',
    'group' => 'experimental',
  );
  $features['install_modules'] = array(
    'title' => t('Install modules'),
    'description' => t('Installs one or more modules automatically after a site is installed.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_install_modules',
    'group' => 'experimental',
  );
  $features['purge'] = array(
    'title' => t('Purge API'),
    'description' => t('Provides common functionality for completely removing sites and platforms..'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_purge',
    'group' => 'experimental',
  );
  $features['purge_platform'] = array(
    'title' => t('Purge Platforms'),
    'description' => t('Provides a Hosting task to completely remove platforms.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_purge_platform',
    'group' => 'experimental',
  );
  $features['purge_site'] = array(
    'title' => t('Purge Sites'),
    'description' => t('Provides a Hosting task to completely remove sites.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_purge_site',
    'group' => 'experimental',
  );

  return $features;
}
