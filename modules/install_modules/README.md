Aegir Install Modules
====================

The 'Install Modules' feature is intended to simplify site-specific customization workflows, by enabling one or more modules after a site is installed.

This Hosting Feature does not expose any front-end components, at the moment. To use it, add a 'install-modules' option to your sites' `local.drushrc.php`:

    $options['install-modules'] = [
      'example_com_content',
    ];

Doing this should, for example, allow `drush @sitename provision-reinstall` to result in a fully configured, freshly installed, local development site. Similarly, building such a site in a test or staging environment, should achieve similar results.
