<?php
/**
 * @file
 */

/**
 * Implements drush_HOOK_post_COMMAND().
 *
 * Enables a set of modules after a site is installed.
 */
function drush_install_modules_post_provision_install() {
  // The local.drushrc.php file has likely changed since the context was
  // initially loaded. So we reload it to refresh global options.
  provision_reload_config('site', d()->site_path . '/drushrc.php');

  $modules = drush_get_option('install-modules', []);
  provision_backend_invoke(d()->name, "pm-enable", $modules, ['yes' => 1]);
}

function drush_install_modules_post_provision_reinstall() {
  drush_install_modules_post_provision_install();
}

