Aegir Purge API
===============

The Purge Hosting Features are intended to allow sites, platforms and other Aegir entities (content-types) to be completely removed from the system, regardless of how "broken" they might be.

Aegir's 'Delete' tasks usually assume that the back-end context (Drush alias) that contains all the relevant data for an Aegir entity is complete and working. If something went wrong during installation or later, and this data is incorrect, incomplete or missing, Aegir will not be able to delete the entity safely.

This is where the 'Purge' tasks come in. These tasks don't make any assumptions about the availability of such context data, though they will use any that is available.


Extending
---------

The Purge API was designed to be both fault-tolerant and easy to extend. The goal was to allow developers to focus on their specific requirements, and minimize any boilerplate. 

As such, the best place to start is by implementing `hook_provision_purge_steps()`. See `drush/provision_purge.api.php` for specifics. But the gist is that you simply list the steps you'll want to take during a purge task. You'll also need to provide a 'weight' value, since the order of execution of purge steps can be crucial to their success. Next run a 'Purge' task, and check the 'simulate' option. The Purge API should prompt you through the rest of the way to having a working implementation.

For illustration purposes, let's look at how we'd extend site purge tasks to remove an S3 bucket. hosting_s3 already provides a Hosting Feature, and so its `drush/provision_s3.drush.inc` file is already bootstrapped by Drush on backend calls. Implementing these is required, but beyond the scope of this documentation. For more info, see the example modules that ship with Aegir core, and the relevant hook documentation that explains these components.

So, assuming we're already bootstrapped properly, we'd start with something like:

    /**
     * Implements hook_provision_purge_steps().
     */
    function provision_s3_provision_purge_steps($type) {
      $module_steps = array();
      if ($type == 'site') {
        $module_steps['provision_s3'] = array(
          'purge_site_bucket' => array('weight' => 10),
        );
      }
      return $module_steps;
    }

After running a purge task on a site, we should see a warning like:

    Purge function provision_s3_purge_site_bucket() has not been implemented.

So, we'll need to implement that function. Let's think about what data we'd have to pass provision_s3_purge_site_bucket(). The basics would be a set of AWS credentials and the name of an S3 bucket. So let's write our function signature accordingly:

    /**
     * Purge step callback to delete an S3 bucket.
     *
     * See: provision_s3_provision_purge_steps().
     */
    function provision_s3_purge_site_bucket($s3_bucket_name, $s3_access_key_id, $s3_secret_access_key) {
      // Do the actual bucket deletion here
    }

We chose these parameter names carefully, as they are saved into the site context for S3-enabled sites. By naming the parameters after the relevant keys in the alias array, if the site context (alias) is available, the Purge API will use the data therein. Re-running the simulated site purge task, should now result some new warnings:

    Context data retrieval function provision_s3_purge_site_bucket_get_s3_bucket_name() has not been implemented.
    Context data retrieval function provision_s3_purge_site_bucket_get_s3_access_key_id() has not been implemented.
    Context data retrieval function provision_s3_purge_site_bucket_get_s3_secret_access_key() has not been implemented.

These function names are built using PHP's Reflection API, by examining the parameters of the purge function we defined earlier. Each such context data retrieval function should be passed a $context variable by reference, to populate the data, if it's missing from the site alias. For example, the first function should look something like this:

    /**
     * Context data retrieval callback to populate the S3 bucket name to be purged.
     *
     * See: provision_s3_purge_site_bucket().
     */
    function provision_s3_purge_site_bucket_get_s3_bucket_name(&$context) {
      // Look up the data we need from a source other than the site alias, and set it in the context.
      // For example, we might pass the data into the task from the front-end, as an option.
      $context['s3_bucket_name'] = drush_get_context('s3_bucket_name', FALSE);
    }

We can then proceed to do similarly for the other two context data retrieval functions. If the site alias doesn't have the relevant data, and we fail to set the correct key in $context, we'll get warnings along the lines of:

    Missing parameter `s3_secret_access_key` for function `provision_s3_purge_site_bucket()`.	
    Skipping purge function `provision_s3_purge_site_bucket()`.

Assuming that either the alias ir valid, or that we've implemented the proper context data retrieval functions, re-running the simulated site purge task, should now no longer display any warnings. Instead, we should see messages along the lines of:

    Dry-run: Here we would have called provision_s3_purge_site_bucket($s3_bucket_name, $s3_access_key_id, $s3_secret_access_key)	
    Where $s3_bucket_name would be set to `...`
    Where $s3_access_key_id would be set to `...`
    Where $s3_secret_access_key would be set to `...`

This allows us to confirm that we have the correct data, prior to proceeding with the purge. Obviously, this could expose some sensitive data in the task log. That said, the task log ought to be deleted, along with the site node when the purge task is executed for real.
