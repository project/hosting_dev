<?php
/**
 * @file Hook documentation for provision_purge.
 */

/**
 * Declare the Aegir entities that your module will be responsible for purging.
 *
 * The returned array should be keyed by the type, and the value should be an
 * optional argument used in cases where the Aegir context/alias is too broken
 * and we'll need to deduce the parameters required for the various purge
 * functions. Passing NULL as the value will disable the optional argument.
 *
 * The Purge API will generate a Drush command for each type (i.e.,
 * 'provision-purge-site', 'provision-purge-platform', etc.)
 *
 * This hook is not required unless you want to be able to purge a content-type
 * that is not yet supported. Even then, should you require additional
 * flexibility, you are free to implement your own Drush command and callback
 * instead.
 */
function hook_provision_purge_types() {
  return array('site' => 'path');
}

/**
 * Declare additional steps that should be executed when purging.
 *
 * Each step will call a function formed by the module (hook) name, and the
 * step key. For the example below, these functions would be:
 *  * provision_s3_purge_site_bucket($s3_creds, $bucket_name)
 *  * provision_s3_purge_backup_buckets($s3_creds, $buckets)
 *
 * Lookup functions will be called for each parameter of each purge function.
 * Each will be passed the $context array by reference in turn. Each such
 * function should populate $context with its required parameter. For the
 * example below, these functions would be:
 *  * provision_s3_purge_site_bucket_get_s3_creds(&$context)
 *  * provision_s3_purge_site_bucket_get_bucket_name(&$context)
 *  * provision_s3_purge_backup_buckets_get_s3_creds(&$context)
 *  * provision_s3_purge_backup_buckets_get_buckets(&$context)
 *
 * The Purge API is designed to be fault tolerant. If any of the expected
 * functions are missing, a warning will be emitted in the task log, and the
 * purge task will continue. When extending the Purge API with additional
 * steps, it's best to start with hook_provision_purge_steps(), and simply
 * implement the functions that are prompted in these warnings. They should
 * provide sufficient information to make the process quite straight-forward.
 */
function hook_provision_purge_steps($type) {
  $module_steps = array();
  if ($type == 'site') {
    $module_steps['provision_s3'] = array(
      'purge_site_bucket' => array('weight' => 10),
      'purge_backup_buckets' => array('weight' => 100),
    );
  }
  return $module_steps;
}

/**
 * Add or alter defined purge steps.
 */
function hook_provision_purge_steps_alter(&$purge_steps, $type) {
  if ($type == 'site') {
    // Verify platform to regenerate sites.php, etc.
    $purge_steps['provision_purge_site']['platform_verify'] = array('weight' => 1000);
  }
}

/**
 * Alter the context variables that will be passed to the purge functions.
 */
function hook_provision_purge_context_alter(&$context, $type) {
  if ($type == 'platform') {
    drush_log('Adding some_var data to platform context.', 'ok');
    $context['some_var'] = 'my_value';
  }
}
