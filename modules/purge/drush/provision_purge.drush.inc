<?php
/**
 * @file Common functionality for Provision Purge commands.
 */

/**
 * Implements hook_drush_command().
 */
function provision_purge_site_drush_command() {
  $types = provision_purge_get_purge_types();
  $items = array();
  foreach ($types as $type => $arg) {
    $map = array('!type' => $type, '!arg' => $arg);
    $items['provision-purge-' . $type] = array(
      'description' => dt('Purge a !type.', $map),
      'examples' => array(
        dt('drush @!type_alias provision-purge-!type', $map) => dt('Force-delete all traces of a !type.', $map),
      ),
      'options' => array(
        'dry-run' => dt('Simulate all relevant actions. Do not actually run purge steps. Will report back the arguments the steps would be run with.'),
      ),
      'callback' => 'drush_provision_purge',
      'callback arguments' => array($type),
      'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
    );
    if (!empty($arg)) {
      $items['provision-purge-' . $type]['arguments'] = array(
        $arg => dt('(Optional) The !arg to fallback on to purge a !type, if the context/alias is too broken.', $map),
      );
      $items['provision-purge-' . $type]['examples'][dt('drush provision-purge-!type <!arg>', $map)] = dt('Force-delete all traces of a !type using the fallback !arg.', $map);
    }
  }
  return $items;
}

/**
 * Return a list of Aegir content types supported by the Purge API extensions.
 *
 * Invokes hook_provision_purge_steps().
 */
function provision_purge_get_purge_types() {
  $types = drush_command_invoke_all('provision_purge_types');
  return $types;
}

/**
 * Drush Provision Purge task callback.
 */
function drush_provision_purge($type, $arg = NULL) {
  provision_purge($type, $arg);
}

/**
 * Return the relevant context: the data required to perform purge steps.
 */
function provision_purge_get_context($type, $arg = NULL) {
  $context = array(
    'purge_name' => provision_purge_get_context_name($arg),
    'purge_type' => $type,
    'purge_' . $arg => $arg,
  );
  provision_purge_fill_context_data($context);
  drush_command_invoke_all_ref('provision_purge_context_alter', $context, $type);
  return $context;
}

/**
 * Fill in context data.
 */
function provision_purge_fill_context_data(&$context) {
  // Merge any data we can retrieve from the alias.
  // @TODO: use '@' to avoid error should the record not exist, or otherwise fail to load?
  $record = provision_sitealias_get_record($context['purge_name']);
  $context += $record;
  $context_funcs = provision_purge_get_context_functions($context['purge_type']);
  foreach ($context_funcs as $func => $arg) {
    if (!array_key_exists($arg, $context)) {
      drush_log(dt('Context data missing. Attempting to deduce: :arg.', array(':arg' => $arg)));
      $result = $func($context);
      if (!$result) {
        return drush_set_error('PROVISION_PURGE_CONTEXT', dt('Failed to deduce :type context data :arg failed at :func.', array(':type' => $context['purge_type'], ':arg' => $arg, ':func' => $func)));
      }
    }
    else {
      drush_log(dt('Found required argument `:arg` in :type context data for `:func()`.', array(':type' => $context['purge_type'], ':arg' => $arg, ':func' => $func)));
    }
  }
}

/**
 * Return the functions required to retrieve context data for a site or platform.
 */
function provision_purge_get_context_functions($type) {
  $purge_funcs = provision_purge_get_purge_functions($type);
  $context_funcs = array();
  foreach ($purge_funcs as $function_name => $reflectionFunction) {
    $params = $reflectionFunction->getParameters();
    foreach ($params as $param) {
      $context_func = provision_purge_get_data_function_name($function_name, $param->name);
      if (function_exists($context_func)) {
        $context_funcs[$context_func] = $param->name;
      }
      else {
        drush_log(dt('Context data retrieval function :func() has not been implemented.', array(':func' => $context_func)), 'warning');
      }
    }
  }
  return $context_funcs;
}

/**
 * Invoke hooks to populate an array of steps required to purge the site or platform.
 *
 * Invokes hook_provision_purge_steps() and hook_provision_purge_steps_alter().
 */
function provision_purge_get_purge_steps($type) {
  $steps = drush_command_invoke_all('provision_purge_steps', $type);
  drush_command_invoke_all_ref('provision_purge_steps_alter', $steps, $type);
  return $steps;
}

/**
 * Return the defined functions required to purge a site or platform.
 */
function provision_purge_get_purge_functions($type) {
  $purge_funcs = array();
  $function_names = provision_purge_get_purge_function_names($type);
  foreach ($function_names as $weight => $function_name) {
    if (function_exists($function_name)) {
      $purge_funcs[$function_name] = new ReflectionFunction($function_name);
    }
    else {
      drush_log(dt('Purge function :func() has not been implemented.', array(':func' => $function_name)), 'warning');
    }
  }
  return $purge_funcs;
}

/**
 * Return all purge function names, ordered by weight.
 */
function provision_purge_get_purge_function_names($type) {
  $module_steps = provision_purge_get_purge_steps($type);
  $purge_funcs = array();
  foreach ($module_steps as $module => $steps) {
    foreach ($steps as $step => $info) {
      $purge_funcs[$info['weight']] = $module . '_' . $step;
    }
  }
  ksort($purge_funcs);
  return $purge_funcs;
}

/**
 * Contruct a data retrieval function name.
 */
function provision_purge_get_data_function_name($function_name, $parameter) {
  return $function_name . '_get_' . $parameter;
}

/**
 * Return the functions required to purge a site or platform.
 */
function provision_purge_get_valid_purge_functions($type, $context) {
  $valid_funcs = array();
  $purge_funcs = provision_purge_get_purge_functions($type);
  foreach ($purge_funcs as $function_name => $reflectionFunction) {
    $params = $reflectionFunction->getParameters();
    $valid_params = array();
    $missing_params = array();
    foreach ($params as $param) {
      if (!array_key_exists($param->name, $context)) {
        $missing_params[] = $param->name;
        drush_log(dt('Missing parameter `:param` for function `:func()`.', array(':param' => $param->name, ':func' => $function_name)), 'warning');
        $data_func = provision_purge_get_data_function_name($function_name, $param->name);
        if (function_exists($data_func)) {
          drush_log(dt('Fix context data retrieval function `:func()`. `:param` needs to be set in $context.', array(':param' => $param->name, ':func' => $data_func)), 'warning');
        }
      }
      else {
        // @TODO: validate that param type also matches. Appears to require PHP 7 for $param->getType().
        // Also, type hinting only works for objects and arrays in PHP. Really? WTF PHP?
        $valid_params[] = $param->name;
      }
    }
    if (empty($missing_params)) {
      $valid_funcs[$function_name] = $valid_params;
    }
    else {
      drush_log(dt('Skipping purge function `:func()`.', array(':func' => $function_name)), 'warning');
    }
  }

  return $valid_funcs;
}

/**
 * Purge a site or platform.
 */
function provision_purge($type, $arg = NULL) {
  // Do not automatically save the drushrc at the end of the command.
  drush_set_option('provision_save_config', false);

  $context = provision_purge_get_context($type, $arg);
  $purge_funcs = provision_purge_get_valid_purge_functions($type, $context);
  drush_log(dt('Purging :type: :name', array(':type' => $type, ':name' => $context['purge_name'])), 'ok');
  foreach ($purge_funcs as $function => $params) {
    $args = array();
    foreach ($params as $param) {
      $args[$param] = $context[$param];
    }
    if (drush_get_option('dry-run', FALSE)) {
      provision_purge_dry_run($function, $args);
    }
    else {
      $result = call_user_func_array($function, $args);
      if (!$result) {
        return drush_set_error('PROVISION_PURGE', dt('Purging :type failed at :func.', array(':type' => $type, ':func' => $func)));
      }
    }
  }
  drush_log(dt('Purged :type: :name', array(':type' => $type, ':name' => $context['purge_name'])), 'success');
}

/**
 * Print the purge steps that would be run.
 */
function provision_purge_dry_run($function, $args) {
  $params = array();
  $print_args = array();
  foreach ($args as $param => $value) {
    if (is_array($value)) {
      $value = implode(', ', $value);
    }
    $params[] = $param;
    $print_args[] = dt('Where $:param would be set to `:value`', array(':param' => $param, ':value' => $value));
  }
  drush_log(dt('Dry-run: Here we would have called :func($:params)', array(':func' => $function, ':params' => implode(', $', $params))), 'ok');
  foreach ($print_args as $print_arg) {
    drush_log($print_arg);
  }
}

/**
 * Look up the context name.
 */
function provision_purge_get_context_name($arg = NULL) {
  // @TODO: Look up the alias directly, in case it's too broken for d() to parse it.
  // @TODO: Fallback to constructing this from a CLI argument, if the alias is missing.
  return d()->name;
}
