<?php
/**
 * @file Documentation of the Hosting Purge API front-end.
 */

/**
 * Declare your Aegir content type(s) as eligible to be purged.
 * 
 * In most cases, this will be all the front-end code required to allow your
 * Aegir content type to be purged. The Hosting Purge API will generate hosting
 * tasks, permissions, and add a confirmation message to the purge task.
 *
 * Note that this hook is only required if you need to add support for purging
 * a new content-type.
 */
function hook_hosting_purge_types() {
  $types = array('site');
  return $types;
}

/**
 * Alter the Aegir content types that can be purged.
 */
function hook_hosting_purge_types_alter($types) {}
