<?php
/**
 * @file Front-end Drush integration.
 */

/**
 * Map values of site node into command line arguments
 */
function drush_hosting_purge_pre_hosting_task($task) {
  $task = &drush_get_context('HOSTING_TASK');

  if (substr($task->task_type, 0, 6) == 'purge-') {
    $task->options['dry-run'] = $task->task_args['dry-run'];
  }
}
