<?php

/**
 * Delete the platform alias.
 */
function provision_purge_platform_drush_alias($alias) {
  $result = TRUE;
  drush_log(dt('Deleting drush alias :alias.', array(':alias' => $alias)), 'ok');
  #$result = drush_invoke_process('@none', 'provision-save', array($alias), array('delete' => TRUE));
  if (!$result) {
    drush_log(dt('Deleted drush alias :alias.', array(':alias' => $alias)), 'success');
  }
  return $result;
}

