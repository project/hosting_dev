<?php

/**
 * Recursively delete the platform root.
 */
function provision_purge_platform_platform_root($root) {
  $result = TRUE;
  if (is_dir($root)) {
    drush_log(dt('Deleting platform root :root', array(':root' => $root)), 'ok');
    // TODO: test this.
    #$result = _provision_recursive_delete($root);
    if (!$result) {
      drush_log(dt('Failed to delete platform root :root', array(':root' => $root)), 'error');
    }
    else {
      drush_log(dt('Deleting platform from remote servers'), 'ok');
      // TODO: test this.
      #$result = d()->service('http')->sync($root);
      if (!$result) {
        drush_log(dt('Failed to delete platform root from remote servers'), 'error');
      }
    }
  }
  return $result;
}

