<?php

/**
 * Purge all the sites on a platform.
 */
function provision_purge_platform_sites(array $sites) {
  $result = TRUE;
  if (count($sites)) {
    drush_log(dt('Purging the following sites: :sites.', array(':sites' => implode(', ', array_keys($sites)))), 'ok');
    $purged = _provision_purge_platform_sites($sites);
    if (!empty($purged)) {
      drush_log(dt('Purged the following sites: :sites.', array(':sites' => implode(', ', array_keys($purged)))), 'success');
    }
    $remaining = array_diff_key($sites, $purged);
    if (count($remaining)) {
      drush_log(dt('Failed to purge the following sites: :sites.', array(':sites' => implode(', ', array_keys($remaining)))), 'error');
      // TODO: Add additional help?
      $result = FALSE;
    }
  }
  return $result;
}

/**
 * Populate the purge context with a list of sites on a platform.
 */
function provision_purge_platform_sites_get_sites(&$context) {
  $path = $context['root'];
  $sites_path = $path . "/sites";
  $status = 'success';
  $result = TRUE;
  drush_log(dt('Scanning Drupal sites directory at :path.', array(':path' => $sites_path)), 'notice');
  if (!is_dir($sites_path)) {
    $status = 'warning';
    drush_log(dt('Sites path does not exist, or is not visible to aegir user.'), $status);
    $result = FALSE;
  }
  elseif ($dir = @opendir($sites_path)) {
    closedir($dir);
    $orig_path = getcwd();
    chdir($path);
    $context['sites'] = provision_drupal_find_sites();
    chdir($orig_path);
  }
  else {
    $status = 'warning';
    drush_log(dt('Could could not scan sites directory'), $status);
    $result = FALSE;
  }
  drush_log(dt('Found :count sites.', array(':count' => count($context['sites']))), $status);
  return $result;
}

function _provision_purge_platform_sites($sites) {
  $purged = array();
  if (count($sites)) {
    // We have to assume the platform won't verify.
    $options = array('skip-verify' => TRUE);
    foreach ($sites as $site => $site_path) {
      $alias = '@' . $site;
      $result = drush_invoke_process($alias, 'provision-purge-site', array(), $options);
      if (!count($result['error'])) {
        $purged[$site] = $site_path;
      }
      else {
        drush_log(dt('Failed to purge :site.', array(':site' => $alias)), 'warning');
        drush_log(dt('Attempting to purge path :path', array(':path' => $site_path)), 'ok');
        $args = array($site_path);
        $result = drush_invoke_process('@none', 'provision-purge-site', $args, $options);
        if (!count($result['error'])) {
          $purged[$site] = $site_path;
        }
        else {
          drush_log(dt('Failed to purge :path. This may cause further issues later in the process.', array(':path' => $site_path)), 'warning');
        }
      }
    }
  }
  return $purged;
}

