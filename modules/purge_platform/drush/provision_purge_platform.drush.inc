<?php
/**
 * @file Purge API hook implementations.
 */

provision_purge_platform_require_includes();

/**
 * Include a file for each defined step.
 */
function provision_purge_platform_require_includes() {
  $steps = provision_purge_platform_provision_purge_steps('platform');
  foreach ($steps['provision_purge_platform'] as $step => $info) {
    require_once(dirname(__FILE__) . '/includes/' . $step . '.inc');
  }
}

/**
 * Implements hook_provision_purge_types().
 */
function provision_purge_platform_provision_purge_types() {
  return array('platform' => 'path');
}

/**
 * Implements hook_provision_purge_steps().
 */
function provision_purge_platform_provision_purge_steps($type) {
  $purge_steps = array();
  if ($type == 'platform') {
    $steps['sites'] = array(
      'weight' => '0',
    );
    $steps['platform_root'] = array(
      'weight' => '10',
    );
    $steps['drush_alias'] = array(
      'weight' => '20',
    );
    # vhosts?
    # verify web server?
    $purge_steps['provision_purge_platform'] = $steps;
  }
  return $purge_steps;
}

