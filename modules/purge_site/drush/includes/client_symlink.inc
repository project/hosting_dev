<?php

/**
 * Delete the symlink in the clients directory.
 */
function provision_purge_site_client_symlink($client_name) {
  $result = TRUE;
  $client_symlink_path = '/var/aegir/clients/' . $client_name;
  if (is_link($client_symlink_path)) {
    drush_log(dt('Deleting client symlink at :path.', array(':path' => $client_symlink_path)), 'ok');
    // TODO: test this.
    #$result = _provision_client_delete_symlink();
    if (!$result) {
      drush_log(dt('Failed to delete client symlink at :path.', array (':path' => $client_symlink_path)), 'warning');
    }
  }
  return $result;
}

