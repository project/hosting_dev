<?php

/**
 * Delete all site Drush aliases.
 */
function provision_purge_site_drush_aliases(array $drush_aliases) {
  $result = TRUE;
  if (count($drush_aliases)) {
    foreach ($drush_aliases as $alias) {
      drush_log(dt('Deleting Drush alias :alias.', array(':alias' => $alias)), 'ok');
      // TODO: test this.
      #$result = drush_invoke_process('@none', 'provision-save', array($alias), array('delete' => TRUE));
      if (!$result) {
        drush_log(dt('Failed to delete Drush alias :alias.', array(':alias' => $alias)), 'error');
      }
    }
  }
  return $result;
}

/**
 * Add a list of Drush aliases to the purge context.
 */
function provision_purge_site_drush_aliases_get_drush_aliases(&$context) {
  $aliases = array();
  if (isset($context['drush_aliases'])) {
    $aliases += $context['drush_aliases'];
  }
  $aliases[] = $context['purge_name'];
  foreach ($aliases as $key => $alias) {
    drush_log(dt('Checking for the existence of :alias alias', array(':alias' => $alias)), 'notice');
    $alias_path = drush_server_home() . '/.drush/' . ltrim($alias, '@') . '.alias.drushrc.php';
    if (!file_exists($alias_path)) {
      drush_log(dt('Alias :alias appears not to exist at :path. Skipping', array(':alias' => $alias, ':path' => $alias_path)), 'notice');
      unset($aliases[$key]);
    }
  }
  $context['drush_aliases'] = $aliases;
}

