<?php

/**
 * Delete the site directory.
 */
function provision_purge_site_site_dir($site_path) {
  $result = TRUE;
  if (is_dir($site_path)) {
    drush_log(dt('Fixing permissions of site path :site_path', array(':site_path' => $site_path)), 'ok');
    // TODO: Implement 'fix-permissions'. See: https://www.drupal.org/node/2616426
    #$result = drush_invoke_process('@' . $alias_name, 'provision-fix-permissions');
    if (!$result) {
      drush_log(dt('Failed to fix permissions of site path :site_path', array(':site_path' => $site_path)), 'error');
    }
    else {
      drush_log(dt('Deleting site path :site_path', array(':site_path' => $site_path)), 'ok');
      // TODO: test this.
      #$result = _provision_recursive_delete($site_path);
      if (!$result) {
        drush_log(dt('Failed to delete site path :site_path', array(':site_path' => $site_path)), 'error');
      }
      else {
        drush_log(dt('Deleting site from remote servers'), 'ok');
        // TODO: test this.
        #$result = provision_drupal_push_site($override_slave_authority = TRUE);
        if (!$result) {
          drush_log(dt('Failed to delete site from remote servers'), 'error');
        }
      }
    }
  }
  return $result;
}

