<?php

/**
 * Delete URL alias symlinks for sites.
 */
function provision_purge_site_url_aliases($aliases) {
  $result = TRUE;
  if (count($aliases)) {
    drush_log(dt('Deleting URL alias symlinks for sites.'), 'ok');
    // TODO: test this.
    #$result = _provision_drupal_delete_aliases();
    if (!$result) {
      drush_log(dt('Failed to delete URL alias symlinks for sites.'), 'error');
    }
  }
  return $result;
}

/**
 * Scan for site sliases and add them to context for purge step.
 */
function provision_purge_site_url_aliases_get_aliases(&$context) {
  drush_log(dt('Scanning for Drupal site URL aliases.'), 'ok');
  $aliases = array();
  // TODO: get site aliases.
  $context['aliases'] = $aliases;
  drush_log(dt('Found :count Drupal site URL aliases.', array(':count' => count($aliases))), 'ok');
}

