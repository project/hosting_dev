<?php
/**
 * @file Purge API hook implementations
 */

provision_purge_site_require_includes();

/**
 * Include a file for each defined step.
 */
function provision_purge_site_require_includes() {
  $steps = provision_purge_site_provision_purge_steps('site');
  foreach ($steps['provision_purge_site'] as $step => $info) {
    require_once(dirname(__FILE__) . '/includes/' . $step . '.inc');
  }
}

/**
 * Implements hook_provision_purge_types().
 */
function provision_purge_site_provision_purge_types() {
  return array('site' => 'path');
}

/**
 * Implements hook_provision_purge_steps().
 */
function provision_purge_site_provision_purge_steps($type) {
  $purge_steps = array();
  if ($type == 'site') {
    $steps['database'] = array(
      'weight' => '0',
    );
    $steps['vhost'] = array(
      'weight' => '10',
    );
    $steps['ssl'] = array(
      'weight' => '20',
    );
    $steps['site_dir'] = array(
      'weight' => '30',
    );
    $steps['url_aliases'] = array(
      'weight' => '40',
    );
    $steps['client_symlink'] = array(
      'weight' => '50',
    );
    $steps['drush_aliases'] = array(
      'weight' => '60',
    );
    $steps['platform_verify'] = array(
      'weight' => '1000',
    );
    $purge_steps['provision_purge_site'] = $steps;
  }
  return $purge_steps;
}
