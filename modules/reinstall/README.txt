The Hosting Reinstall module provides a Drush command and Hosting task
so that you can easily reinstall sites in Aegir.

It was originally written for the purpose of testing Drupal install profiles.
It is also useful for development feature/git-based workflows.

Hosting Reinstall was originally written Stuart Clark and maintained by Stuart
Clark (deciphered) of Realityloop Pty Ltd.
- http://www.realityloop.com
- http://twitter.com/realityloop

The port to Aegir 3 and its on-going maintenance is by Christopher Gervais
(ergonlogic).
