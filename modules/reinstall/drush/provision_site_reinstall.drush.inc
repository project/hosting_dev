<?php
/**
 * @file
 */

define('PROVISION_RESTORE_ITEMS', 'modules,themes,libraries,local.settings.php,local.drushrc.php');

/**
 * Implements hook_drush_command().
 */
function provision_site_reinstall_drush_command() {
  $items = array();

  $items['provision-reinstall'] = array(
    'description' => 'Reinstall a site.',
    'examples' => array(
      'drush @site provision-reinstall' => 'Invokes Provision Delete and Install tasks to simulate a site reinstall.',
    ),
    'options' => array(
      'restore-items' => 'A list of files and directories to restore from the pre-delete backup. Defaults to "' . PROVISION_RESTORE_ITEMS . '".',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT
  );

  return $items;
}

/**
 * Drush Provision Reinstall task callback.
 *
 * Invokes Provision Delete and Install tasks to simulate a site reinstall.
 */
function drush_provision_site_reinstall_provision_reinstall() {
  // Preserve our existing Aegir site context data, since it gets deleted along
  // with ths site.
  $alias_name = d()->name;
  $options = provision_sitealias_get_record($alias_name);

  $result = provision_backend_invoke($alias_name, "provision-delete");
  $backup = $result['context']['backup_file'];

  // Recreate our Aegir site context.
  provision_backend_invoke("@hostmaster", "provision-save", array($alias_name), $options);

  provision_backend_invoke($alias_name, "provision-install");

  // Restore specified files and directories from backup.
  $restore_items = drush_get_option('restore-items', PROVISION_RESTORE_ITEMS);
  if ($restore_items) {
    $items = explode(',', $restore_items);
    drush_log(dt('Restoring `:items` from backup :backup.', array(':items' => $restore_items, ':backup' => $backup)));
    foreach ($items as $item) {
      provision_reinstall_extract_path($backup, $item, $options);
    }
  }
}

/**
 * Extract a path from a backup
 */
function provision_reinstall_extract_path($backup, $item, $context) {
  $args = array(
    ':backup' => $backup,
    ':item' => $item,
    ':dest' => $context['site_path'] . '/' . $item,
  );
  $check = 'tar -tf %s ./%s';
  $command = 'tar -xvf %s -C %s ./%s';

  $check_result = drush_shell_exec($check, $backup, $item);
  if ($check_result) {
    drush_log(dt('Found path `:item` in backup.', $args), 'info');
    $result = drush_shell_exec($command, $backup, $context['site_path'], $item);
    if ($result) {
      drush_log(dt('Extracted `:item` from :backup to :dest.', $args), 'success');
    }
    else {
      drush_log(dt('Failed to extract `:item` from :backup to :dest.', $args), 'warning');
      drush_log(dt("Failure output:\n :output", array(':output' => implode("\n", drush_shell_exec_output()))), 'warning');
    }
  }
  else {
    drush_log(dt('Path `:item` not found in backup. Skipping.', $args), 'info');
  }
}
